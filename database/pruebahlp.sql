-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 27-04-2020 a las 21:43:38
-- Versión del servidor: 10.1.33-MariaDB
-- Versión de PHP: 7.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pruebahlp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacoras`
--

CREATE TABLE `bitacoras` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `acceso` datetime NOT NULL,
  `ip` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `bitacoras`
--

INSERT INTO `bitacoras` (`id`, `id_usuario`, `acceso`, `ip`) VALUES
(1, 10, '0000-00-00 00:00:00', '0'),
(2, 10, '2020-04-27 19:26:29', '::1'),
(3, 10, '2020-04-27 19:27:50', '::1'),
(4, 10, '2020-04-27 19:29:58', '::1'),
(5, 10, '2020-04-27 19:31:03', '::1'),
(6, 10, '2020-04-27 19:36:42', '::1'),
(7, 10, '2020-04-27 19:37:49', '::1'),
(8, 12, '2020-04-27 21:24:06', '::1'),
(9, 10, '2020-04-27 21:24:33', '::1'),
(10, 10, '2020-04-27 21:40:39', '::1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(175) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(175) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contrasena` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_alta` datetime NOT NULL,
  `tipo_usuario` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellidos`, `email`, `contrasena`, `telefono`, `direccion`, `fecha_alta`, `tipo_usuario`, `status`) VALUES
(10, 'Hector', 'Lopez', 'hectorcoreylp@gmail.com', '5ea702aebf420', '5591451616', 'Cuautitlan', '0000-00-00 00:00:00', 0, 1),
(11, 'Victor', 'Flores', 'vic@gmail.com', '5ea7039a9fd8f', '5591451616', 'Cuautitlan', '2020-04-27 16:08:58', 1, 1),
(12, 'Hector', 'Lopez Palma', 'hectorcoreyhlp@gmail.com', '5ea72641bd251', '5591451616', 'San Sebastián Xhala,Cuautitlán Izcalli,México', '2020-04-27 18:36:49', 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bitacoras`
--
ALTER TABLE `bitacoras`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bitacoras`
--
ALTER TABLE `bitacoras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
