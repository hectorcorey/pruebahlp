<?php
class Usuario extends CI_Model
{
  public $table = 'usuarios';
  public $table_id = 'id';

  function __construct()
  {

  }
  public function find($id)
  {
    $this->db->select();
    $this->db->from($this->table);
    $this->db->where($this->table_id,$id);
    $query = $this->db->get();
    return $query->row();
  }
  public function findLogin($email)
  {
    $this->db->select();
    $this->db->from($this->table);
    $this->db->where("email",$email);
    $query = $this->db->get();
    return $query->row();
  }
  public function findAll()
  {
    $this->db->select();
    $this->db->from($this->table);
    $query = $this->db->get();
    return $query->result();
  }
  public function findAccesos($id,$fecha_inicio,$fecha_fin)
  {
    $where = "usuarios.id = '".$id."' AND bitacoras.acceso >='".$fecha_inicio."  00:00:00' AND bitacoras.acceso <= '".$fecha_fin."  23:59:59'";
    return $this->db
      ->select('usuarios.nombre, usuarios.apellidos, usuarios.email, bitacoras.acceso, bitacoras.ip')
      ->from($this->table)
      ->join('bitacoras', 'usuarios.id = bitacoras.id_usuario')
      ->where($where)
      ->get()
      ->result();
  }
  public function insert($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }
  public function update($id, $data)
  {
    $this->db->update($this->table,$data);
    $this->db->where($this->table_id, $id);
  }
  public function delete($id)
  {
    $this->db->delete($this->table);
    $this->db->where($this->table_id, $id);
  }

}

 ?>
