<?php
class Bitacora extends CI_Model
{
  public $table = 'bitacoras';
  public $table_id = 'id';

  function __construct()
  {

  }
  public function find($id)
  {
    $this->db->select();
    $this->db->from($this->table);
    $this->db->where($this->table_id,$id);
    $query = $this->db->get();
    return $query->row();
  }
  public function insert($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

}

 ?>
