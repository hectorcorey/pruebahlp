<?php

class Usuarios extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->helper('form');
     $this->load->library('form_validation');
    $this->load->database();
    $this->load->model('Usuario');
    $this->load->helper('url');
    $this->load->library("session");
    if (!isset($this->session->id_user)) {
      redirect(base_url('Login'));
    }
  }
  public function create()
  {
    if ($this->session->tipo_usuario != 0) {
      redirect(base_url('Home/index'));
    }
    $this->layout->view('guardar');
  }
  public function store($value='')
  {
    if ($this->session->tipo_usuario != 0) {
      redirect(base_url('Home/index'));
    }
    if($this->input->server("REQUEST_METHOD") == "POST"){

      $data['nombre'] = $this->input->post("nombre");
      $data['apellidos'] = $this->input->post("apellidos");
      $data['email'] = $this->input->post("email");
      $data['contrasena'] = $this->generatePassword();
      $data['telefono'] = $this->input->post("telefono");
      $data['direccion'] = $this->input->post("colonia") .",".$this->input->post("municipio").",".$this->input->post("estado");
      $data['fecha_alta'] = date("Y-m-d H:i:s");
      $data['tipo_usuario'] = $this->input->post("tipo_usuario");
      $data['status'] = $this->input->post("status");

      if ( $this->validarDatos() == false) {
          $this->layout->view('guardar');
      } else {
        $add = $this->Usuario->insert($data);
        $this->sendEmail($data['email'],$data['nombre'],$data['contrasena']);
          echo "Datos cargador correctamente";
        if($add==true){
          $this->session->set_flashdata('correcto', 'Usuario añadido correctamente');
        }else{
          $this->session->set_flashdata('incorrecto', 'Usuario no añadido');
        }
        redirect(base_url('Usuarios/create'));
      }
    }
  }
  public function detalle()
  {
    $datos['user'] = $this->Usuario->find($this->session->id_user);
    $this->layout->view('detalle',$datos);
  }
  public function generatePassword()
  {
    return uniqid();
  }
  public function sendEmail($email,$nombre,$contrasena)
  {
    $this->load->library('email');

    $config['protocol'] = 'smtp';
    $config["smtp_host"] = 'smtp.gmail.com';
    $config["smtp_user"] = 'pruebahlpp@gmail.com';
    $config["smtp_pass"] = 'pruebahlp2020';
    $config['smtp_timeout'] = '7';
    $config["smtp_crypto"] = 'ssl';
    $config["smtp_port"] = '465';
    $config['charset'] = 'utf-8';
    $config['newline']    = "\r\n";
    $config['mailtype'] = 'text';
    $config['validation'] = TRUE;

    $this->email->initialize($config);

    $this->email->from('pruebahlpp@gmail.com', 'Prueba Técnica');
        $this->email->to($email, $nombre);

      //Definimos el asunto del mensaje
        $this->email->subject("Asignacion de contraseña");

      //Definimos el mensaje a enviar
        $this->email->message(
                "Email: ".$email.
                " Contraseña: " . $contrasena.
                " Pagina: ".base_url()
                );
        if($this->email->send()){
            $this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            $this->session->set_flashdata('noenvio', 'No se a enviado el email');
        }
    }
  public function validarDatos()
  {
    $this->form_validation->set_rules('nombre', 'Nombre', 'required|min_length[3]|alpha_numeric_spaces');
    $this->form_validation->set_rules('apellidos', 'Apellidos', 'required|min_length[3]|alpha_numeric_spaces');
    $this->form_validation->set_rules('email', 'Email', 'required|min_length[3]|valid_email|is_unique[usuarios.email]');
    $this->form_validation->set_rules('telefono', 'Teléfono', 'required|min_length[3]');
    $this->form_validation->set_rules('colonia', 'Colonia', 'required');
    $this->form_validation->set_rules('municipio', 'Delegación/Municipio', 'required');
    $this->form_validation->set_rules('estado', 'Estado', 'required');
    $this->form_validation->set_rules('tipo_usuario', 'Tipo de Usuario', 'required');
    $this->form_validation->set_rules('status', 'Status', 'required');

    $this->form_validation->set_message('required','El campo %s es obligatorio');
    $this->form_validation->set_message('alpha','El campo %s debe estar compuesto solo por letras');
    $this->form_validation->set_message('min_length[3]','El campo %s debe tener mas de 3 caracteres');
    $this->form_validation->set_message('valid_email','El campo %s debe ser un email correcto');
    $this->form_validation->set_message('is_unique','El %s tiene un registro existente');

    return $this->form_validation->run();
  }
}

 ?>
