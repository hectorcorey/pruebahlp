<?php

class Reportes extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->helper('form');
    $this->load->database();
    $this->load->helper('url');
    $this->load->model('Usuario');
    $this->load->library("session");
    if (!isset($this->session->id_user)) {
      redirect(base_url('Login'));
    }
  /*  if ($this->session->tipo_usuario) {
      redirect(base_url('Home/index'));
    }*/
  }
  public function index()
  {
    $this->layout->view('filtrado');

  }
  public function filtrado()
  {
    if ($this->session->tipo_usuario != 0) {
      redirect(base_url('Home/index'));
    }
    $datos['users'] = $this->Usuario->findAll();
    if($this->input->server("REQUEST_METHOD") == "POST"){
      $datos['user_id']= $this->input->post("user");
      $datos['fecha_ini']= $this->input->post("fecha_ini");
      $datos['fecha_fin']= $this->input->post("fecha_fin");
      $datos['bitacora'] = $this->Usuario->findAccesos($datos['user_id'],$datos['fecha_ini'], $datos['fecha_fin']);
      $datos['btn'] = true;
      $this->layout->view('filtrado',$datos);
    }else{
      $this->layout->view('filtrado',$datos);
    }
  }
  public function filtradoExcel($user,$fecha_ini,$fecha_fin)
  {
    if ($this->session->tipo_usuario != 0) {
      redirect(base_url('Home/index'));
    }
    $registros = $this->Usuario->findAccesos($user,$fecha_ini,$fecha_fin);
    if(count($registros) > 0){
      //Cargamos la librería de excel.
      $this->load->library('excel'); 
      $this->excel->setActiveSheetIndex(0);
      $this->excel->getActiveSheet()->setTitle('Bitacora de inicio de sesion');
      //Contador de filas
      $contador = 1;
      //Ancho las columnas.
      $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
      $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
      $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
      $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
      //Negrita a los títulos de la cabecera.
      $this->excel->getActiveSheet()->getStyle("A{$contador}")->getFont()->setBold(true);
      $this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
      $this->excel->getActiveSheet()->getStyle("C{$contador}")->getFont()->setBold(true);
      $this->excel->getActiveSheet()->getStyle("C{$contador}")->getFont()->setBold(true);
      //Títulos de la cabecera.
      $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'Nombre');
      $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Correo');
      $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Acceso');
      $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'IP');
      //Definimos la data del cuerpo.        
      foreach($registros as $r){
         $contador++;
         $this->excel->getActiveSheet()->setCellValue("A{$contador}", $r->nombre.' '.$r->apellidos);
         $this->excel->getActiveSheet()->setCellValue("B{$contador}", $r->email);
         $this->excel->getActiveSheet()->setCellValue("C{$contador}", $r->acceso);
         $this->excel->getActiveSheet()->setCellValue("D{$contador}", $r->ip);
      }
      //Le ponemos un nombre al archivo que se va a generar.
      $archivo = "bitacora_user_{$user}_fechas.xls";
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'.$archivo.'"');
      header('Cache-Control: max-age=0');
      $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
      //Hacemos una salida al navegador con el archivo Excel.
      $objWriter->save('php://output');
   }else{
      echo 'No se han encontrado Registros';
      exit;        
   }
  }
  public function lista()
  {
    if ($this->session->tipo_usuario != 0) {
      redirect(base_url('Home/index'));
    }
    $datos['users'] = $this->Usuario->findAll();
    $this->layout->view('listausuarios',$datos);

  }
  public function listaExcel()
  {
    if ($this->session->tipo_usuario != 0) {
      redirect(base_url('Home/index'));
    }
    $registros = $this->Usuario->findAll();
    if(count($registros) > 0){
      //Cargamos la librería de excel.
      $this->load->library('excel'); 
      $this->excel->setActiveSheetIndex(0);
      $this->excel->getActiveSheet()->setTitle('Bitacora de inicio de sesion');
      //Contador de filas
      $contador = 1;
      //Ancho las columnas.
      $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
      $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
      $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
      $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(100);
      //Negrita a los títulos de la cabecera.
      $this->excel->getActiveSheet()->getStyle("A{$contador}")->getFont()->setBold(true);
      $this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
      $this->excel->getActiveSheet()->getStyle("C{$contador}")->getFont()->setBold(true);
      $this->excel->getActiveSheet()->getStyle("C{$contador}")->getFont()->setBold(true);
      //Títulos de la cabecera.
      $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'Nombre');
      $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Correo');
      $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Teléfono');
      $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Dirección');
      //Definimos la data del cuerpo.        
      foreach($registros as $r){
         $contador++;
         $this->excel->getActiveSheet()->setCellValue("A{$contador}", $r->nombre.' '.$r->apellidos);
         $this->excel->getActiveSheet()->setCellValue("B{$contador}", $r->email);
         $this->excel->getActiveSheet()->setCellValue("C{$contador}", $r->telefono);
         $this->excel->getActiveSheet()->setCellValue("D{$contador}", $r->direccion);
      }
      //Le ponemos un nombre al archivo que se va a generar.
      $archivo = "listado_usuarios.xls";
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'.$archivo.'"');
      header('Cache-Control: max-age=0');
      $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
      //Hacemos una salida al navegador con el archivo Excel.
      $objWriter->save('php://output');
   }else{
      echo 'No se han encontrado Registros';
      exit;        
   }
  }
  public function bitacora()
  {
    if($this->input->server("REQUEST_METHOD") == "POST"){
      $datos['accesos'] = $this->Usuario->findAccesos($this->session->id_user,$this->input->post("fecha_ini"),$this->input->post("fecha_fin"));
      $this->layout->view('bitacorausuario',$datos);
    }else{
      $this->layout->view('bitacorausuario');
    }
    

  }

}

 ?>
