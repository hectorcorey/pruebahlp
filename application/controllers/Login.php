<?php
class Login extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->helper('form');
    $this->load->database();
    $this->load->model('Usuario');
    $this->load->model('Bitacora');
    $this->load->helper('url');
    $this->load->library("session");
  }
  public function index()
  {
    if (isset($this->session->id_user)) {
      redirect(base_url('Inicio'));
    }else {
      $this->load->view('Login/login');
    }
  }
  public function login()
  {
    $email = $this->input->post("email");
    $contrasena = $this->input->post("contrasena");
    $user = $this->Usuario->findLogin($email);
    if ($user != Null) {
      if ($user->contrasena == $contrasena && $user->status == 1) {
        $this->session->email = $user->email;
        $this->session->id_user = $user->id;
        $this->session->nombre = $user->nombre;
        $this->session->tipo_usuario = $user->tipo_usuario;
        $this->Bitacora->insert(["id_usuario" => $user->id, 'acceso' => date("Y-m-d H:i:s"),'ip' => $this->input->ip_address()]);
        redirect(base_url('Home/index'));
      }else {
        $this->session->set_flashdata('incorrecto', 'Credenciales incorrectas');
        redirect(base_url('Login'));
      }
    }
    else {
      $this->session->set_flashdata('incorrecto', 'No existe usuario');
      redirect(base_url('Login'));
    }
  }
  public function logout()
  {
    session_destroy();
    $this->load->view('Login/login');
  }

}

 ?>
