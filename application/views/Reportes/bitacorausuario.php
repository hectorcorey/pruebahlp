<form class="needs-validation mt-5" method="post" action="<?php echo base_url('Reportes/bitacora') ?>">
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label>Fecha Inicio</label>
      <input type="date" class="form-control" name="fecha_ini" required>
    </div>
    <div class="col-md-4 mb-3">
      <label>Fecha Fin</label>
        <input type="date" class="form-control" name="fecha_fin" required>
    </div>
  </div>
  <button class="btn btn-primary" type="submit">Buscar</button>
</form>

<table class="table mt-5">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Acceso</th>
        <th scope="col">IP</th>
    </tr>
  </thead>
  <tbody>
  <?php if(isset($accesos)):?>
  <?php foreach($accesos as $acceso): ?>
  <tr>
    <td><?php echo $acceso->acceso;?></td>
    <td><?php echo $acceso->ip;?></td>
  </tr>
<?php endforeach;?>
  <?php endif;?>

  </tbody>
</table>
