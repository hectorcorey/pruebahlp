<form class="needs-validation mt-5" method="post" action="<?php echo base_url('Reportes/filtrado') ?>">
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationTooltip01">Usuario</label>
      <select class="custom-select" name="user">
      <?php foreach($users as $user): ?>
          <option value="<?php echo $user->id;?>"><?php echo $user->nombre;?> <?php echo $user->apellidos;?></option>
      <?php endforeach;?>
      </select>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationTooltip02">Fecha Inicio</label>
      <input type="date" class="form-control" name="fecha_ini" required>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationTooltipUsername">Fecha Fin</label>
        <input type="date" class="form-control" name="fecha_fin" required>
    </div>
  </div>
  <button class="btn btn-primary" type="submit">Buscar</button>
  
</form>

<table class="table mt-5">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Correo</th>
      <th scope="col">Acceso</th>
        <th scope="col">IP</th>
    </tr>
  </thead>
  <tbody>
<?php if(isset($bitacora)):?>
  <?php foreach($bitacora as $bita): ?>
  <tr>
    <td ><?php echo $bita->nombre;?> <?php echo $bita->apellidos;?></td>
    <td><?php echo $bita->email;?></td>
    <td><?php echo $bita->acceso;?></td>
    <td><?php echo $bita->ip;?></td>
  </tr>
<?php endforeach;?>
  <?php endif;?>
  </tbody>
</table>
<?php if(isset($btn)):?>
<a href="<?php echo base_url('excel_filtrado/'. $user_id.'/'.$fecha_ini.'/'.$fecha_fin) ?>" target="_blank" class="btn btn-primary">Exportar Excel</a>
<?php endif;?>