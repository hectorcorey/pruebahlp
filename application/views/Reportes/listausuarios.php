<a href="<?php echo base_url('Reportes/listaExcel');?>" class="btn btn-primary mt-5">Exportar Excel</a>
<table class="table mt-5">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Correo</th>
      <th scope="col">Teléfono</th>
        <th scope="col">Dirección</th>
    </tr>
  </thead>
  <tbody>
  <?php if(isset($users)):?>
  <?php foreach($users as $user): ?>
  <tr>
    <td ><?php echo $user->nombre;?> <?php echo $user->apellidos;?></td>
    <td><?php echo $user->email;?></td>
    <td><?php echo $user->telefono;?></td>
    <td><?php echo $user->direccion;?></td>
  </tr>
<?php endforeach;?>
  <?php endif;?>

  </tbody>
</table>
