<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">Prueba Técnica</a>

  <div class="navbar-collapse" style="padding-right:90px;">
    <div class="navbar-nav mr-auto "></div>
    <ul class="navbar-nav">
      <?php if (isset($this->session->id_user)): ?>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         <?php echo $this->session->nombre; ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo base_url('Usuarios/detalle'); ?>">Datos Personales</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url('Login/logout') ?>">Cerrar Sesión</a>
        </div>
      <?php else: ?>
      </li>
      <li class="nav-item">
          <a class="nav-link active" href="<?php echo base_url('Login'); ?>">Iniciar Sesion</a>
        </li>
        <?php endif; ?>
    </ul>
  </div>
</nav>
<?php if (isset($this->session->id_user)): ?>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 bg-light sidebar" >
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <?php if ($this->session->tipo_usuario == 0): ?>


              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('Usuarios/create'); ?>">

                  Registro de usuarios
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('Reportes/filtrado'); ?>">

                  Reporte de bitácoras de los usuarios
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('Reportes/lista'); ?>">
                Reporte de usuarios
                </a>
              </li>
              <?php endif; ?>
              <?php if ($this->session->tipo_usuario == 1): ?>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('Reportes/bitacora'); ?>">
                Reporte de bitacoras
                </a>
              </li>
            <?php endif; ?>

            </ul>
          </div>
        </nav>
      <?php endif; ?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
      <?php
        echo $content_for_layout;
      ?>
      </main>
      </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $("#codigo_postal").change(function(){
          var codigo = $("#codigo_postal").val();
            $.ajax({
                // cargamos url a nuestro contralador y método indicado
                url:"https://api-sepomex.hckdrk.mx/query/info_cp/"+codigo+"?type=simplified",
                type:"get",
                success:function(data){
                    if(data){
                        if (!data.error) {
                          var html = "";
                          for (var i = 0; i < data.response.asentamiento.length; i++) {
                            html += '<option value="'+data.response.asentamiento[i]+'">'+data.response.asentamiento[i]+'</option>'
                          }
                          $('#colonia').html(html);
                          $('#municipio').val(data.response.municipio);
                          $('#estado').val(data.response.estado);
                        }
                        console.log(data.response);
                    }
                    else{
                        alert("error")
                    }
                }
            })
        })
    })

    </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
