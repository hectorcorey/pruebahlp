      <?php
        //Si existen las sesiones flasdata que se muestren
            if($this->session->flashdata('correcto'))
                echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
                  '.$this->session->flashdata('correcto').'
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>';

            if($this->session->flashdata('incorrecto'))
                echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                  '.$this->session->flashdata('incorrecto').'
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>';

            if($this->session->flashdata('envio'))
            echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
              '.$this->session->flashdata('envio').'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';

            if($this->session->flashdata('noenvio'))
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
              '.$this->session->flashdata('envio').'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
        ?>


        <?php
          if (validation_errors()!= '')
          echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                  '.validation_errors().'
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>'; ?>
        <h1>Registro de Usuarios</h1>
        <?php echo form_open('Usuarios/store'); ?>
    <div class="form-group">
      <?php
      echo form_label('Nombre','nombre');
      $data = array(
        'name'  => 'nombre',
        'value' => set_value('nombre'),
        'class' => 'form-control input-lg',
        'placeholder' => 'Introducir nombre'
      );
      echo form_input($data);
       ?>

       <?php
       echo form_label('Apellidos','apellidos');
       $data = array(
         'name'  => 'apellidos',
         'value' => set_value('apellidos'),
         'class' => 'form-control input-lg',
         'placeholder' => 'Introducir apellidos'
       );
       echo form_input($data);
        ?>

        <?php
        echo form_label('Email','email');
        $data = array(
          'type' => 'email',
          'name'  => 'email',
          'value' => set_value('email'),
          'class' => 'form-control input-lg',
          'placeholder' => 'Ejemplo@email.com'
        );
        echo form_input($data);
         ?>

         <?php
         echo form_label('Teléfono','telefono');
         $data = array(
           'name'  => 'telefono',
           'value' => set_value('telefono'),
           'class' => 'form-control input-lg',
           'placeholder'=>'55 5555 5555'
         );
         echo form_input($data);
          ?>
         <?php
         echo form_label('Codigo Postal','cp');
         $data = array(
           'id'=> 'codigo_postal',
           'name'  => 'cp',
           'value' => set_value('cp'),
           'class' => 'form-control input-lg',
           'placeholder'=>'00000',
         );
         echo form_input($data);
          ?>

         <?php
         echo form_label('Direccion','direccion');
          echo form_label('Colonia','colonia');
          $options = array(
            );
            $js = array(
              'class' => 'custom-select',
              'id' => 'colonia'
          );
            echo form_dropdown('colonia', $options, '1', $js);
            echo form_label('Delegación/Municipio');
            $data = array(
              'id'=> 'municipio',
              'name'  => 'municipio',
              'value' => set_value('municipio'),
              'class' => 'form-control input-lg',
              'readonly' =>'true'
            );
            echo form_input($data);
            echo form_label('Estado');
            $data = array(
              'id'=> 'estado',
              'name'  => 'estado',
              'value' => set_value('estado'),
              'class' => 'form-control input-lg',
              'readonly' =>'true'
            );
            echo form_input($data);
          ?>


          <?php
          echo form_label('Tipo de Usuario','tipo_usuario');
          $options = array(
            '0'         => 'Administrador',
            '1'           => 'Usuario',
            );
            $js = array(
              'class' => 'custom-select',
      );
            echo form_dropdown('tipo_usuario', $options, '1', $js);
          ?>
          <?php
          echo form_label('Status','status');
          $options = array(
            '0' => 'inactivo',
            '1' => 'activo',
            );
            $js = array(
              'class' => 'custom-select',
      );
            echo form_dropdown('status', $options, '1', $js);
          ?>
          <?php
          $js = 'class="btn btn-info"';
          echo form_submit('mysubmit', 'Guardar',$js);
          ?>
    </div>
    <?php echo form_close(); ?>
